---
title: "About the author"
---

I am a data engineer at [Vase.ai](https://vase.ai/), a market analytics platform that collects insights through automated delivery of surveys to the mass market. 

I program primarily in:
- Javascript 
- R
- Haskell 
- R / Python
- Elm

I currently think about:
- How to learn and understand Haskell 
- Balanced Sampling in business

Open-source projects I am working on:
- [Aitchison](https://github.com/tintinthong/aitchison): A typescript library in Aitchison Geometry.
- [CadCad](): A simulation model for voting networks

