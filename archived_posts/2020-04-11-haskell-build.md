---
title: "Haskell Builds"
tags: haskell
---

<p>
 <!-- <a href="MY WEBSITE LINK" target="_blank"> -->
 <img src="/img/grod.jpg" style='width:80%; display:block; margin-left:auto;margin-right:auto' border="0" alt="Null">
 <!-- </a> -->
</p>

In this article, I want to talk about how to get a project running in Haskell. This here task was intidmidating at the very least to me. I knew how to code in Haskell, but the unfamiliar build tools make it difficult to make any progress on a project.

- What is stack? 

Stack is a software that helps you 
it manages dependencies helps you build your project helps install ghc in isolated location 
The concept of Stack lies more upon the Stackage. Stackage is a collection of packages in the Haskell ecosystem that work together. Stackage keeps snapshots (ie collection of packages which work together ) along with version of ghc. 
The idea is unlike npm since npm enables you to install repositories to their latest version as modified in pacakge.json. Whereas, stack has an LTS and its best to required dependencies within the LTS snapshot. 

- What was the workflow before?  

The reason we had to have a solution for stack is because of cabal hell.  Cabal stores all dependencies inside a global db. However, there are problems with this. Its not a very unique problem, its a problem more like cabal just does not (CANNOT) resolve your dependencies.  

The user db is just slapped on top of global db. (Where can I find this?) 

 `cabal-install` which is the equivalent of `npm install` does not handle dependencies in a practical way. For instance, 

https://www.haskell.org/cabal/FAQ.html#dependencies-conflict
https://github.com/haskell/cabal/wiki/Dependency-Resolution
https://github.com/haskell/cabal/wiki/Constraint-Solving

If B and C which are dpendencies of A are dependent of different versions of D respectively. If A uses B and C relatably, it will not be able there may be conflicts. So in this example, the dependency of which D to use is not resolved.  

This then leads to need for something like stack . As cabal is also being used at more regulated ecosystems, however, hackage which is the package repository, literally has no ecossytem. 

This forces people to always need to resolve these MANUALLY. All beginnners will jsut pull their hair and wonder why dafuq isn't it just like npm. 

This is also the reason  of new things such as unison.  

- Does it mean that cabal sucks? There are some solutiosn? 





https://wiki.haskell.org/Cabal/Survival
https://stackoverflow.com/questions/50925938/what-is-cabal-hell

-  What is the difference between Stack and Cabal? 

https://docs.haskellstack.org/en/stable/faq/#what-is-the-relationship-between-stack-and-cabal


- Can I just use Stack for everything?

Of course this doesn't mean that you are restricted to just using a parituclar versio nof package dependencies. You can specify `extra-deps` within your stack.yaml (local) and your `.cabal` file. This enables you to just build from packages.  

if there is no `stack.yaml` and only a `.cabal` file. You might wanna consider just `stack init`.

https://stackoverflow.com/questions/33446558/understanding-haskells-stack-program-and-the-resolver-and-lts-version

https://lexi-lambda.github.io/blog/2018/02/10/an-opinionated-guide-to-haskell-in-2018/

https://www.well-typed.com/blog/2008/04/the-dreaded-diamond-dependency-problem/

https://medium.com/learnwithrahul/understanding-npm-dependency-resolution-84a24180901b
