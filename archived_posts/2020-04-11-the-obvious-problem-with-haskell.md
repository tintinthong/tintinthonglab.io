---
title: "The Obvious Problem with Haskell"
tags: haskell
---

I was looking for problems to solve in developing package repository.
I realised there are so many barriers to entering the haskell ecosystem. 

It became clear to me that ideas spur a lot more movement than coding. Haskellers are coding machines. They love their head-down, most likely came from comp sci background so they know a lot more about architecture, compilers, rolls more off the tongue for them. Haskellers are a lot more competent statistic, also because they are more technical. In Javascript and Python communities, most people just learn it, they probably have no notion of any types... they just know `brew install npm` and `npm install`. Heck im good to go. 

I couldn't even find a workflow for Haskell, whereas the workflow for javascript is either on the `README.md` of any file and every tutorial that is known to man. Can we have a workflow of `stack` and `cabal` and `nix`.   

It is true that there is a sense of ivery towerness. There is a sense that all this jargon in Haskell Lens, Monads and Functional IO comes from a birth place of academia and it does discourage people from even using it in the same place. And don't get me started on cabal hell. 

But my observation is that Haskell people are not like this.

It doesn't lack community becaues there are devout followers. But, it does lack quantity of simple documents. Simo Ahava for example is source of almost all analytics. 

It seems that anyone who knows Haskell either is too pre-occupied with sorting out Haskell by themselves that they never bother to pass down the knowledge. 

I am not saying that Haskellers are not inviting. 

It is similar to R. And although R is a domain-specific language, I believe Haskell has the capacity to move into a move a more popular usage rate like R. It can act as similar to R. Hadley Wickham has been the centralised person when it comes to all things R-related. That is because there is a non-profit that backs the development. 

And I refer to this reddit when a person suggested that there should be. No I actually do not think that we lack centralised documents. This problem is isomorphic to Math Anxiety. When people put things in simple way, it just fucking works. 
I mean building a package in itself is already difficult. And people do not have the capacity to be learning Haskell with no quick answers. 

It lacks usability
There is a high barrier to entry

By writing this blog, I want to close the gap. Make it a simple place where people can come for Haskell advice. Streamline the workflow to imitate something like javascript or python. So expect a style that brings all technicality down. We need more writing. Better writing. Yup better writing. Thats my solution. More FAQs. More writing. More on to pedagogy than research.  

Stephe Diehl wrote a north-star type documentation.  

http://dev.stephendiehl.com/nearfuture.pdf
