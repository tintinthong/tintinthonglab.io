---
title: "When I see a DO"
tags: haskell
---

In Haskell, you will often see `do` notations -- EVERYWHERE!. Particularly, when performing IO operations. There is a lot going underneath the hood for `do` notation. `do` notation as you might have guessed is syntactical sugar for Monads.  

```
showName :: String -> Maybe String
showName name = do
  y <- Just "!"
  Just show $ x ++ y
```

`showName(name)` is a function that prints adds a BANG symbol at the end of the name. However, the function returns "name !" inside of a contextual wrapper (`Just`). 

```
Just 3 >>= (\x -> Just (show x ++ "!"))  
```